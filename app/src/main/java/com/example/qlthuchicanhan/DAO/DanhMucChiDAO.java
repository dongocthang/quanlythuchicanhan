package com.example.qlthuchicanhan.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.qlthuchicanhan.Model.DanhMucChi;

import java.util.List;

@Dao
public interface DanhMucChiDAO {
    @Insert
    void insert(DanhMucChi danhMucChi);

    @Insert
    void insertList(List<DanhMucChi> list);

    @Query("delete from danhmucchi")
    void deleteAll();

    @Query("select * from danhmucchi where id_user = :id")
    List<DanhMucChi> getList(int id);

    //biểu đồ
    @Query("select * from danhmucchi where id = :id_danhmucchi")
    List<DanhMucChi> danhSachTheoId(int id_danhmucchi);
}
