package com.example.qlthuchicanhan.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.qlthuchicanhan.Model.User;

import java.util.List;

@Dao
public interface UserDAO {
    @Insert
    void insertUser(User user);

    @Query("select * from user where username = :username and password = :password")
    User login(String username, String password);

    @Query("select * from user where username =:username")
    List<User> checkExistsUser(String username);

    @Query("select * from user where id = :id_user")
    List<User> getUser(int id_user);

    @Update
    void UpdatePassword(User user);
}
