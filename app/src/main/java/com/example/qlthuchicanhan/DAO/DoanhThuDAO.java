package com.example.qlthuchicanhan.DAO;

import androidx.annotation.Nullable;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.qlthuchicanhan.Model.Date;
import com.example.qlthuchicanhan.Model.DoanhThu;
import com.example.qlthuchicanhan.Model.ItemDoanhThu;
import com.example.qlthuchicanhan.Model.ItemKhoanChi;

import java.util.List;

@Dao
public interface DoanhThuDAO {

    @Insert
    void insert(DoanhThu doanhThu);

    @Query("update doanhthu set id_danhmucthu = :id_danhmucthu, sotien = :sotien, " +
            "ngaythu = :ngaythu, ghichu = :ghichu, id_user = :id_user where id = :id")
    void update(int id_danhmucthu, long sotien, String ngaythu, String ghichu, int id_user, int id);

    @Query("Delete from doanhthu where id = :id")
    void delete(int id);

    @Query("select * from doanhthu where id_user = :id_user")
    List<DoanhThu> getList(int id_user);

    @Query("select ngaythu from doanhthu where id_user = :id_user")
    List<Date> getAllDate(int id_user);

    @Query("select doanhthu.id, image, name, ghichu, sotien, ngaythu, doanhthu.id_user, " +
            "doanhthu.id_danhmucthu from danhmucthu inner join doanhthu " +
            "on doanhthu.id_danhmucthu = danhmucthu.id where doanhthu.ngaythu = :ngaythu and " +
            "doanhthu.id_user = :id_user and danhmucthu.id_user = :id_user")
    List<ItemDoanhThu> getListItemDoanhThu(String ngaythu, int id_user);

    // biểu đồ
    @Query("select doanhthu.id, image, name, ghichu, sotien, ngaythu, doanhthu.id_user, " +
            "doanhthu.id_danhmucthu from danhmucthu inner join doanhthu " +
            "on doanhthu.id_danhmucthu = danhmucthu.id where doanhthu.id_user = :id_user " +
            "and danhmucthu.id_user = :id_user")
    List<ItemDoanhThu> BieuDo(int id_user);
}
