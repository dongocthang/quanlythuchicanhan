package com.example.qlthuchicanhan.DAO;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.qlthuchicanhan.Model.Date1;
import com.example.qlthuchicanhan.Model.ItemDoanhThu;
import com.example.qlthuchicanhan.Model.ItemKhoanChi;
import com.example.qlthuchicanhan.Model.KhoanChi;

import java.util.List;

@Dao
public interface KhoanChiDAO {
    @Insert
    void insert(KhoanChi khoanChi);

    @Query("update khoanchi set id_danhmucchi = :id_danhmucchi, sotien = :sotien, " +
            "ngaychi = :ngaychi, ghichu = :ghichu, id_user = :id_user where id = :id")
    void update(int id_danhmucchi, long sotien, String ngaychi, String ghichu, int id_user, int id);

    @Query("Delete from khoanchi where id = :id")
    void delete(int id);

    @Query("select * from khoanchi where id_user = :id_user")
    List<KhoanChi> getList(int id_user);

    @Query("select ngaychi from khoanchi where id_user = :id_user")
    List<Date1> getAllDate(int id_user);

    @Query("select khoanchi.id, image, name, ghichu, sotien, ngaychi, khoanchi.id_user, " +
            "khoanchi.id_danhmucchi from danhmucchi inner join khoanchi " +
            "on khoanchi.id_danhmucchi = danhmucchi.id where khoanchi.ngaychi = :ngaychi and " +
            "khoanchi.id_user = :id_user and danhmucchi.id_user = :id_user")
    List<ItemKhoanChi> getListItemKhoanChi(String ngaychi, int id_user);

    // biểu đồ
    @Query("select khoanchi.id, image, name, ghichu, sotien, ngaychi, khoanchi.id_user, " +
            "khoanchi.id_danhmucchi from danhmucchi inner join khoanchi " +
            "on khoanchi.id_danhmucchi = danhmucchi.id where khoanchi.id_user = :id_user " +
            "and danhmucchi.id_user = :id_user")
    List<ItemKhoanChi> BieuDo(int id_user);
}
