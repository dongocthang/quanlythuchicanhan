package com.example.qlthuchicanhan.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.Model.User;

import java.util.List;

@Dao
public interface DanhMucThuDAO {
    @Insert
    void insert(DanhMucThu danhMucThu);

    @Insert
    void insertList(List<DanhMucThu> list);

    @Query("delete from danhmucthu")
    void deleteAll();

    @Query("select * from danhmucthu where id_user = :id")
    List<DanhMucThu> getList(int id);

    //biểu đồ
    @Query("select * from danhmucthu where id = :id_danhmucthu")
    List<DanhMucThu> danhSachTheoId(int id_danhmucthu);
}
