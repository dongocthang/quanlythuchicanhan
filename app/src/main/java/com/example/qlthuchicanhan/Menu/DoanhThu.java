package com.example.qlthuchicanhan.Menu;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlthuchicanhan.Adapter.ItemDoanhThuNgayAdapter;
import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Dialog.ThemMoiDoanhThu;
import com.example.qlthuchicanhan.Dialog.ChonNam;
import com.example.qlthuchicanhan.Model.Date;
import com.example.qlthuchicanhan.Model.ItemDoanhThu;
import com.example.qlthuchicanhan.Model.ItemDoanhThuNgay;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.DuLieuThangNam;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DoanhThu extends Fragment implements DuLieuThangNam {
    TextView tv_thoigian, tv_tongTienThang;
    ImageView img_tien, img_lui;
    RecyclerView rcv_listDoanhThu;
    ItemDoanhThuNgayAdapter doanhThuNgayAdapter;
    FloatingActionButton btnAdd;
    View view;

    private int id_user = -1;
    private int month = -1;
    private int year = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.doanhthu, container, false);
        this.id_user = DataLocalManager.getUser().getId();
        init();

        Calendar calendar = Calendar.getInstance();
        int nam = calendar.get(Calendar.YEAR);
        int thang = calendar.get(Calendar.MONTH);
        String text = formatThang(thang + 1) + "/" + nam;
        //đặt dữ liệu ban đầu cho textview
        tv_thoigian.setText(text);
        tv_thoigian.setOnClickListener(v -> {
            ChonNam chonNam = new ChonNam();
            chonNam.show(getChildFragmentManager(), "Chon Nam");
        });

        this.month = thang + 1;
        this.year   = nam;

        clickImageViewTien();
        clickImageViewLui();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ThemMoiDoanhThu dialog = new ThemMoiDoanhThu();
                dialog.show(getChildFragmentManager(), "Datetime_Dialog");
            }
        });

        doanhThuNgayAdapter = new ItemDoanhThuNgayAdapter(getContext());
        doanhThuNgayAdapter.setData(getListItemDoanhThuNgay());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                RecyclerView.VERTICAL, false);
        rcv_listDoanhThu.setLayoutManager(linearLayoutManager);
        rcv_listDoanhThu.setAdapter(doanhThuNgayAdapter);

        return view;
    }

    //tham chiếu đến các View
    private void init(){
        btnAdd = view.findViewById(R.id.floatbtnAdd);
        tv_thoigian = view.findViewById(R.id.tv_ThoiGian);
        tv_tongTienThang = view.findViewById(R.id.tv_tongTienThang);
        img_tien = view.findViewById(R.id.img_tien);
        img_lui = view.findViewById(R.id.img_lui);
        rcv_listDoanhThu = view.findViewById(R.id.recyclerViewListDoanhThu);
    }

    // format tháng VD: 5 -> 05
    private String formatThang(int thang){
        String text = null;
        if(thang < 10){
            text = "0" + thang;
            return text;
        }
        else{
            return String.valueOf(thang);
        }
    }

    private void clickImageViewTien() {
        img_tien.setOnClickListener(v -> {
            String tam = tv_thoigian.getText().toString();
            String[] mang = tam.split("/");
            int thang = Integer.parseInt(mang[0]);
            int nam = Integer.parseInt(mang[1]);
            if(thang == 12){
                nam  +=  1;
                thang = 1;
            }
            else{
                thang += 1;
            }
            tv_thoigian.setText(formatThang(thang) + "/" + nam);
            this.month = thang;
            this.year   = nam;
            refresh();
        });
    }

    private void clickImageViewLui() {
        img_lui.setOnClickListener(v -> {
            String tam = tv_thoigian.getText().toString();
            String[] mang = tam.split("/");
            int thang = Integer.parseInt(mang[0]);
            int nam = Integer.parseInt(mang[1]);
            if(thang == 1){
                nam  -=  1;
                thang = 12;
            }
            else{
                thang -= 1;
            }
            tv_thoigian.setText(formatThang(thang) + "/" + nam);
            this.month = thang;
            this.year   = nam;
            refresh();
        });
    }


    private String formatNgay(int ngay){
        String text = null;
        if(ngay < 10){
            text = "0" + ngay;
            return text;
        }
        else{
            return String.valueOf(ngay);
        }
    }

    private List<ItemDoanhThuNgay> getListItemDoanhThuNgay() {
        List<ItemDoanhThuNgay> list = new ArrayList<>();

        List<Date> list1 = UserDatabase.getInstance(getContext()).doanhThuDAO().getAllDate(this.id_user);
        Set<String> set = new HashSet<>();
        if(!list1.isEmpty()){
            for(int i = 0; i < list1.size(); i++){
                String tam = list1.get(i).getNgaythu();  // VD: 06/05/2022, 08/05/2022
                int a = Integer.parseInt(tam.substring(0, 2)); // 14
                int b = Integer.parseInt(tam.substring(3, 5)); // 5
                int c = Integer.parseInt(tam.substring(6));    // 2022, month = 5, year = 2022
                if(b == this.month && c == this.year){
                    String text = formatNgay(a) + "/" + formatThang(b) + "/" + c;
                    set.add(text);
                }
            }
        }
        List<Date> list2 = new ArrayList<>();
        for (String s : set) {
            Date d = new Date(s);
            list2.add(d);
        }
        // sắp xếp ngày trong tháng theo chiều giảm dần
        for(int i = 0; i < list2.size()-1; i++){
            for(int j = i + 1; j < list2.size(); j++){
                int a = Integer.parseInt(list2.get(i).getNgaythu().substring(0,2));
                int b = Integer.parseInt(list2.get(j).getNgaythu().substring(0,2));
                if(a < b){
                    Date temp = list2.get(i);
                    list2.set(i, list2.get(j));
                    list2.set(j, temp);
                }
            }
        }

        long tongTienThang = 0;
        if(!list2.isEmpty()){
            for (int i = 0; i < list2.size(); i++) {
                String date = list2.get(i).getNgaythu(); // 05/06/2022
                List<ItemDoanhThu> listItemNgay = UserDatabase.getInstance(getContext()).doanhThuDAO()
                        .getListItemDoanhThu(date, this.id_user);
                long tongtienngay = 0;
                for (int j = 0; j < listItemNgay.size(); j++) {
                    tongtienngay += listItemNgay.get(j).getSotien();
                }
                tongTienThang += tongtienngay;
                ItemDoanhThuNgay item = new ItemDoanhThuNgay(list2.get(i).getNgaythu(), tongtienngay,
                        listItemNgay);
                list.add(item);
            }
        }
        tv_tongTienThang.setText(String.valueOf(tongTienThang) + " đ");
        return list;
    }

    // nhận tháng và năm từ dialog chọn tháng, năm gửi về
    @Override
    public void getThangNam(int thang, int nam) {
        String text = formatThang(thang) + "/" + nam;
        tv_thoigian.setText(text);
        this.month = thang;
        this.year   = nam;
        refresh();
    }

    public void checkClickButtonLuu(int a){
        if(a == 1){
            refresh();
        }
    }

    public void XuLyAfterDeleteItem(int kt){
        if(kt == 1){
            refresh();
        }
    }


    // làm mới dữ liệu cho RecyclerView
    public void refresh(){
        doanhThuNgayAdapter.setData(getListItemDoanhThuNgay());
        rcv_listDoanhThu.setAdapter(doanhThuNgayAdapter);
    }
}
