package com.example.qlthuchicanhan.Menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Dialog.SettingPassword;
import com.example.qlthuchicanhan.MainActivity;
import com.example.qlthuchicanhan.Model.User;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.Sign_In;
import com.example.qlthuchicanhan.TruyenDuLieu.AfterDangXuat;
import com.example.qlthuchicanhan.TruyenDuLieu.DataAfterDeleteOrHuyItem;

import java.util.List;

public class Khac extends Fragment {

    private View view;
    private Button btnLogout;
    private TextView tvfullname;
    private TextView textViewSettingPassword;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.khac,container,false);
        init();
        User user = DataLocalManager.getUser();
        if(user != null){
            tvfullname.setText(user.getFullname());
        }
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logout();
            }
        });
        textViewSettingPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingPassword settingPassword = new SettingPassword();
                settingPassword.show(getChildFragmentManager(),"SettingPassword_dialog");
            }
        });

        return  view;
    }

    private void Logout() {
        new android.app.AlertDialog.Builder(getContext())
                .setMessage("Bạn muốn đăng xuất?")
                .setCancelable(false)
                .setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DataLocalManager.setUser(null);
                        Intent intent = new Intent(getContext(),Sign_In.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Hủy", null)
                .show();
    }

    private void init(){
        btnLogout   = view.findViewById(R.id.btnLogout);
        tvfullname    = view.findViewById(R.id.textViewFullName);
        textViewSettingPassword  = view.findViewById(R.id.textViewSetting);
    }
}
