package com.example.qlthuchicanhan.Model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "khoanchi")
public class KhoanChi {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int id_danhmucchi;
    private long sotien;
    private String ngaychi;
    private String ghichu;
    private int id_user;

    public KhoanChi(int id_danhmucchi, long sotien, String ngaychi, String ghichu, int id_user) {
        this.id_danhmucchi = id_danhmucchi;
        this.sotien = sotien;
        this.ngaychi = ngaychi;
        this.ghichu = ghichu;
        this.id_user = id_user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_danhmucchi() {
        return id_danhmucchi;
    }

    public void setId_danhmucchi(int id_danhmucchi) {
        this.id_danhmucchi = id_danhmucchi;
    }

    public long getSotien() {
        return sotien;
    }

    public void setSotien(long sotien) {
        this.sotien = sotien;
    }

    public String getNgaychi() {
        return ngaychi;
    }

    public void setNgaychi(String ngaychi) {
        this.ngaychi = ngaychi;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
}
