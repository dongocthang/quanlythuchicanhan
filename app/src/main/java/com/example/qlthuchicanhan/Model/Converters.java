package com.example.qlthuchicanhan.Model;

import androidx.room.TypeConverter;
import java.sql.Date;

public class Converters {
    @TypeConverter
    public static Date fromTimestamp(String value) {
        return value == null ? null : new Date(Long.parseLong(value));
    }

    @TypeConverter
    public static String dateToTimestamp(Date date) {
        return String.valueOf(date == null ? null : date.getTime());
    }
}
