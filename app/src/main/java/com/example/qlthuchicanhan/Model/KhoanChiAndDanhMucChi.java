package com.example.qlthuchicanhan.Model;

import androidx.room.Embedded;
import androidx.room.Relation;

public class KhoanChiAndDanhMucChi {
    @Embedded public KhoanChi khoanChi;
    @Relation(
            parentColumn = "id",
            entityColumn = "id_danhmucchi"
    )
    public DanhMucChi danhMucChi;
}
