package com.example.qlthuchicanhan.Model;

import java.util.List;

public class ItemKhoanChiNgay {

    private String date;
    private long tien;
    private List<ItemKhoanChi> itemKhoanChis;

    public ItemKhoanChiNgay(String date, long tien, List<ItemKhoanChi> itemKhoanChis) {
        this.date = date;
        this.tien = tien;
        this.itemKhoanChis = itemKhoanChis;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTien() {
        return tien;
    }

    public void setTien(long tien) {
        this.tien = tien;
    }

    public List<ItemKhoanChi> getItemKhoanChis() {
        return itemKhoanChis;
    }

    public void setItemKhoanChis(List<ItemKhoanChi> itemKhoanChis) {
        this.itemKhoanChis = itemKhoanChis;
    }
}
