package com.example.qlthuchicanhan.Model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class UserWithDanhMucThu {
    @Embedded public User user;
    @Relation(
            parentColumn = "id",
            entityColumn = "id_user"
    )
    public List<DanhMucThu> list;
}
