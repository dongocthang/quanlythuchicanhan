package com.example.qlthuchicanhan.Model;

import java.io.Serializable;

public class ItemDoanhThu implements Serializable {

    private int id;
    private int image;
    private String name;
    private String ghichu;
    private long sotien;
    private String ngaythu;
    private int id_user;
    private int id_danhmucthu;

    public ItemDoanhThu(int id, int image, String name, String ghichu, long sotien, String ngaythu,
                        int id_user, int id_danhmucthu) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.ghichu = ghichu;
        this.sotien = sotien;
        this.ngaythu = ngaythu;
        this.id_user = id_user;
        this.id_danhmucthu = id_danhmucthu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public long getSotien() {
        return sotien;
    }

    public void setSotien(long sotien) {
        this.sotien = sotien;
    }

    public String getNgaythu() {
        return ngaythu;
    }

    public void setNgaythu(String ngaythu) {
        this.ngaythu = ngaythu;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_danhmucthu() {
        return id_danhmucthu;
    }

    public void setId_danhmucthu(int id_danhmucthu) {
        this.id_danhmucthu = id_danhmucthu;
    }
}
