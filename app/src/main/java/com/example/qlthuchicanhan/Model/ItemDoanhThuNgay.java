package com.example.qlthuchicanhan.Model;

import java.util.List;

public class ItemDoanhThuNgay {

    private String date;
    private long tien;
    private List<ItemDoanhThu> itemDoanhThus;

    public ItemDoanhThuNgay(String date, long tien, List<ItemDoanhThu> itemDoanhThus) {
        this.date = date;
        this.tien = tien;
        this.itemDoanhThus = itemDoanhThus;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getTien() {
        return tien;
    }

    public void setTien(long tien) {
        this.tien = tien;
    }

    public List<ItemDoanhThu> getItemDoanhThus() {
        return itemDoanhThus;
    }

    public void setItemDoanhThus(List<ItemDoanhThu> itemDoanhThus) {
        this.itemDoanhThus = itemDoanhThus;
    }
}
