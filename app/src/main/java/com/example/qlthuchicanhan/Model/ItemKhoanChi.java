package com.example.qlthuchicanhan.Model;

import java.io.Serializable;

public class ItemKhoanChi implements Serializable {

    private int id;
    private int image;
    private String name;
    private String ghichu;
    private long sotien;
    private String ngaychi;
    private int id_user;
    private int id_danhmucchi;

    public ItemKhoanChi(int id, int image, String name, String ghichu, long sotien, String ngaychi,
                        int id_user, int id_danhmucchi) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.ghichu = ghichu;
        this.sotien = sotien;
        this.ngaychi = ngaychi;
        this.id_user = id_user;
        this.id_danhmucchi = id_danhmucchi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public long getSotien() {
        return sotien;
    }

    public void setSotien(long sotien) {
        this.sotien = sotien;
    }

    public String getNgaychi() {
        return ngaychi;
    }

    public void setNgaychi(String ngaychi) {
        this.ngaychi = ngaychi;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_danhmucchi() {
        return id_danhmucchi;
    }

    public void setId_danhmucchi(int id_danhmucchi) {
        this.id_danhmucchi = id_danhmucchi;
    }
}
