package com.example.qlthuchicanhan.Model;

public class Date {

    private String ngaythu;

    public Date(String ngaythu) {
        this.ngaythu = ngaythu;
    }

    public String getNgaythu() {
        return ngaythu;
    }

    public void setNgaythu(String ngaythu) {
        this.ngaythu = ngaythu;
    }
}
