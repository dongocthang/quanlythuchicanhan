package com.example.qlthuchicanhan.Model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Relation;

import com.example.qlthuchicanhan.Menu.DoanhThu;

public class DoanhThuAndDanhMucThu {
    @Embedded public DoanhThu doanhThu;
    @Relation(
            parentColumn = "id",
            entityColumn = "id_danhmucthu"
    )
    public DanhMucThu danhMucThu;
}
