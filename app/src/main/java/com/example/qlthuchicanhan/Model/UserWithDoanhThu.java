package com.example.qlthuchicanhan.Model;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.qlthuchicanhan.Menu.DoanhThu;

import java.util.List;

public class UserWithDoanhThu {
    @Embedded public User user;
    @Relation(
            parentColumn = "id",
            entityColumn = "id_user"
    )
    public List<DoanhThu> list;
}
