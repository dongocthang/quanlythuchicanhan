package com.example.qlthuchicanhan.Model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "danhmucchi")
public class DanhMucChi {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private int image;
    private int id_user;

    public DanhMucChi(String name, int image, int id_user) {
        this.name    = name;
        this.image   = image;
        this.id_user = id_user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
}
