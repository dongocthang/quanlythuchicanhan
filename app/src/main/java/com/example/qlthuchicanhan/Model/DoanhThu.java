package com.example.qlthuchicanhan.Model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "doanhthu")
public class DoanhThu {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int id_danhmucthu;
    private long sotien;
    private String ngaythu;
    private String ghichu;
    private int id_user;

    public DoanhThu(int id_danhmucthu, long sotien, String ngaythu, String ghichu, int id_user) {
        this.id_danhmucthu = id_danhmucthu;
        this.sotien = sotien;
        this.ngaythu = ngaythu;
        this.ghichu = ghichu;
        this.id_user = id_user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_danhmucthu() {
        return id_danhmucthu;
    }

    public void setId_danhmucthu(int id_danhmucthu) {
        this.id_danhmucthu = id_danhmucthu;
    }

    public long getSotien() {
        return sotien;
    }

    public void setSotien(long sotien) {
        this.sotien = sotien;
    }

    public String getNgaythu() {
        return ngaythu;
    }

    public void setNgaythu(String ngaythu) {
        this.ngaythu = ngaythu;
    }

    public String getGhichu() {
        return ghichu;
    }

    public void setGhichu(String ghichu) {
        this.ghichu = ghichu;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }
}
