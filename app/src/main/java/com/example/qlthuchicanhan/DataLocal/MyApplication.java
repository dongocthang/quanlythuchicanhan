package com.example.qlthuchicanhan.DataLocal;

import android.app.Application;

public class MyApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        DataLocalManager.Init(getApplicationContext());
    }
}
