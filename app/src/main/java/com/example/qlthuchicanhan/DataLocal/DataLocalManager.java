package com.example.qlthuchicanhan.DataLocal;

import android.content.Context;

import com.example.qlthuchicanhan.Model.User;
import com.google.gson.Gson;

public class DataLocalManager {

    private static final String PREFERENCES_OBJECT_USER = "PREFERENCES_OBJECT_USER";
    private static DataLocalManager instance;
    private MySharedPreferences mySharedPreferences;

    public static void Init(Context context){
        instance = new DataLocalManager();
        instance.mySharedPreferences = new MySharedPreferences(context);
    }

    public static DataLocalManager getInstance(){
        if(instance == null){
            instance = new DataLocalManager();
        }
        return instance;
    }

    public static void setUser(User user){
        Gson gson = new Gson();
        String jsonUser = gson.toJson(user);
        DataLocalManager.getInstance().mySharedPreferences.putStringUserValue(PREFERENCES_OBJECT_USER, jsonUser);
    }
    public static User getUser(){
        String jsonUser = DataLocalManager.getInstance().mySharedPreferences.getStringUserValue(PREFERENCES_OBJECT_USER);
        Gson gson = new Gson();
        User user = gson.fromJson(jsonUser,User.class);
        return user;
    }

}
