package com.example.qlthuchicanhan;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;


import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.User;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Sign_Up extends AppCompatActivity {
    TextView Back_signIN, goSignIN;

    EditText edFullname, edUsername, edPassword;
    Button btSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        edFullname = findViewById(R.id.fullname);
        edUsername = findViewById(R.id.username1);
        edPassword = findViewById(R.id.password1);
        btSignup = findViewById(R.id.BTLSign_up);

        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterUser();
            }
        });

        Back_signIN = findViewById(R.id.back_signIN);
        goSignIN = findViewById(R.id.go_signIN);


        Back_signIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Sign_Up.this, Sign_In.class);
                startActivity(i);
            }
        });

        goSignIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Sign_Up.this, Sign_In.class);
                startActivity(i);
            }
        });
    }

    private void RegisterUser() {
        String username = edUsername.getText().toString().trim();
        String password = edPassword.getText().toString().trim();
        String fullname = edFullname.getText().toString().trim();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || TextUtils.isEmpty(fullname)) {
            Toast.makeText(this, "Enter input please", Toast.LENGTH_SHORT).show();
            return;
        }
        User user = new User(username, password, fullname);
        if(isExists(user) == true){
            new SweetAlertDialog(Sign_Up.this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Message")
                    .setContentText("Username already exists!")
                    .setConfirmText("OK")
                    .show();
            return;
        }
        UserDatabase.getInstance(this).userDAO().insertUser(user);
        new SweetAlertDialog(Sign_Up.this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Message")
                .setContentText("Đăng kí thành công!")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent i = new Intent(Sign_Up.this, Sign_In.class);
                        startActivity(i);
                    }
                }).show();

    }
    private boolean isExists(User user){
        List<User> list = UserDatabase.getInstance(this).userDAO().checkExistsUser(user.getUsername());
        return list != null && !list.isEmpty();
    }
}