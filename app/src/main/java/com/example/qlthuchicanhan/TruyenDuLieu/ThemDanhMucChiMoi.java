package com.example.qlthuchicanhan.TruyenDuLieu;

import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;

public interface ThemDanhMucChiMoi {
    void themMoiDanhMucChi(DanhMucChi danhMucChi);
}
