package com.example.qlthuchicanhan;

import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Fragment.BieuDo;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.ItemDoanhThu;
import com.example.qlthuchicanhan.Model.ItemKhoanChi;
import com.example.qlthuchicanhan.Model.KhoanChi;
import com.example.qlthuchicanhan.Model.User;
import com.example.qlthuchicanhan.TruyenDuLieu.DuLieuTongTien;

import org.eazegraph.lib.models.PieModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PieChart extends AppCompatActivity implements DuLieuTongTien {

    Button btnQuayLai;
    CheckBox cbChiTieu, cbThuNhap;
    Button btnChonThoiGian;
    TextView tv_GioiThieu, tv_TongTien;
    DatePickerDialog.OnDateSetListener setListener;

    private int id_user;
    private int month, year;
    private int status = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bieudo);

        id_user = DataLocalManager.getUser().getId();

        init();
        tv_GioiThieu.setText("");
        tv_TongTien.setText("");
        setOnClickCheckBox();
        clickButtonQuayLai();
        clickButtonChonThoiGian();
    }

    private void init() {
        btnQuayLai = findViewById(R.id.btn_quaylai);
        cbChiTieu = findViewById(R.id.cb_chiTieu);
        cbThuNhap = findViewById(R.id.cb_thuNhap);
        btnChonThoiGian = findViewById(R.id.btn_ChonThoiGian);
        tv_GioiThieu = findViewById(R.id.tv_gioiThieu);
        tv_TongTien  = findViewById(R.id.tv_TongTien);
    }

    private void clickButtonQuayLai(){
        btnQuayLai.setOnClickListener(v -> {
            this.finish();
        });
    }

    private void setOnClickCheckBox(){
        cbChiTieu.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(cbChiTieu.isChecked() == false){
                cbThuNhap.setChecked(true);
                status = 2;
            }
            else {
                cbThuNhap.setChecked(false);
                status = 1;
            }
        });
        cbThuNhap.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(cbThuNhap.isChecked() == false){
                cbChiTieu.setChecked(true);
                status = 1;
            }
            else {
                cbChiTieu.setChecked(false);
                status = 2;
            }
        });
    }

    private void clickButtonChonThoiGian() {
        Calendar calendar = Calendar.getInstance();
        int thang = calendar.get(Calendar.MONTH);
        int nam = calendar.get(Calendar.YEAR);
        int ngay = calendar.get(Calendar.DATE);

        btnChonThoiGian.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth, setListener, nam, thang, ngay);
            datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            //loại bỏ ngày, chỉ hiển thị tháng và năm
            ((ViewGroup) datePickerDialog.getDatePicker()).findViewById(Resources.getSystem()
                    .getIdentifier("day", "id", "android")).setVisibility(View.GONE);
            datePickerDialog.show();
        });
        this.setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                PieChart.this.month = month + 1;
                PieChart.this.year  = year;
                Bundle bundle = new Bundle();
                bundle.putInt("id_user", PieChart.this.id_user);
                bundle.putInt("status", PieChart.this.status);
                bundle.putInt("thang", (month + 1));
                bundle.putInt("nam", year);
                BieuDo bieuDo = new BieuDo();
                bieuDo.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.relative_ChuaBieuDo, bieuDo).commit();
            }
        };
    }

    // nhận tổng tiền của tháng trong năm
    @Override
    public void getTongTien(long tongTien) {
        if(tongTien == 0){
            tv_GioiThieu.setText("");
            tv_TongTien.setText("");
        }
        else{
            if(this.status == 1){
                tv_GioiThieu.setText("Biểu đồ thống kê chi tiêu tháng " + this.month + " năm " + this.year);
                tv_TongTien.setText("Tổng Chi " + tongTien + " vnđ");
                tv_TongTien.setTextColor(Color.RED);
            }
            else{
                tv_GioiThieu.setText("Biểu đồ thống kê thu nhập tháng " + this.month + " năm " + this.year);
                tv_TongTien.setText("Tổng Thu: " + tongTien + " vnđ");
                tv_TongTien.setTextColor(Color.rgb(31, 194, 93));
            }
        }
    }
}
