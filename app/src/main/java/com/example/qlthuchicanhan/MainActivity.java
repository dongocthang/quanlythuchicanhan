package com.example.qlthuchicanhan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Menu.DoanhThu;
import com.example.qlthuchicanhan.Menu.Khac;
import com.example.qlthuchicanhan.Menu.Khoanchi;
import com.example.qlthuchicanhan.Menu.Thongke;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.Model.KhoanChi;
import com.example.qlthuchicanhan.Model.User;
import com.example.qlthuchicanhan.TruyenDuLieu.AfterDangXuat;
import com.example.qlthuchicanhan.TruyenDuLieu.DataAfterDeleteOrHuyItem;
import com.example.qlthuchicanhan.TruyenDuLieu.LoadDuLieuDoanhThu;
import com.example.qlthuchicanhan.TruyenDuLieu.LoadDuLieuKhoanChi;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoadDuLieuDoanhThu,
        LoadDuLieuKhoanChi, DataAfterDeleteOrHuyItem, AfterDangXuat {

    private int id_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        id_user = DataLocalManager.getUser().getId();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        DoanhThu doanhThu = new DoanhThu();
        getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, doanhThu).commit();

        addDanhMucThu();
        addDanhMucChi();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;

                    switch (item.getItemId()) {
                        case R.id.Doanhthu:
                            selectedFragment = new DoanhThu();
                            break;
                        case R.id.Khoanchi:
                            selectedFragment = new Khoanchi();
                            break;
                        case R.id.Thongke:
                            selectedFragment = new Thongke();
                            break;
                        case R.id.Khac:
                            selectedFragment = new Khac();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.Fragment_container, selectedFragment).commit();
                    return true;
                }
            };

    public void addDanhMucThu(){
        List<DanhMucThu> list = new ArrayList<DanhMucThu>();
        list.add(new DanhMucThu("Cho thuê nhà", R.drawable.icon_chothuenha, this.id_user));
        list.add(new DanhMucThu("Quà tặng", R.drawable.icon_gif, this.id_user));
        list.add(new DanhMucThu("Trợ cấp", R.drawable.icon_trocap, this.id_user));
        list.add(new DanhMucThu("Việc làm", R.drawable.icon_vieclam, this.id_user));
        list.add(new DanhMucThu("Buôn bán", R.drawable.icon_buonban, this.id_user));
        list.add(new DanhMucThu("Thu nhập chính", R.drawable.icon_thunhapchinh, this.id_user));
        list.add(new DanhMucThu("Lãi suất", R.drawable.icon_laisuat, this.id_user));
        list.add(new DanhMucThu("Sổ xố", R.drawable.icon_soxo, this.id_user));

        if(UserDatabase.getInstance(this).danhMucThuDAO().getList(this.id_user).size() == 0){
            UserDatabase.getInstance(this).danhMucThuDAO().insertList(list);
        }
        return;
    }

    public void addDanhMucChi(){
        List<DanhMucChi> list = new ArrayList<>();
        list.add(new DanhMucChi("Grab,Uber", R.drawable.icon_grab_uber, this.id_user));
        list.add(new DanhMucChi("Mua sắm", R.drawable.icon_muasam, this.id_user));
        list.add(new DanhMucChi("Sức khỏe", R.drawable.icon_suckhoe, this.id_user));
        list.add(new DanhMucChi("Xe buýt", R.drawable.icon_xebuyt, this.id_user));
        list.add(new DanhMucChi("Vé máy bay", R.drawable.icon_vemaybay, this.id_user));
        list.add(new DanhMucChi("Cho vay", R.drawable.icon_chovay, this.id_user));
        list.add(new DanhMucChi("Kinh doanh", R.drawable.icon_kinhdoanh, this.id_user));
        list.add(new DanhMucChi("Ăn uống", R.drawable.icon_anuong, this.id_user));
        list.add(new DanhMucChi("Điện nước", R.drawable.icon_diennuoc, this.id_user));
        list.add(new DanhMucChi("Con cái", R.drawable.icon_concai, this.id_user));
        list.add(new DanhMucChi("Quần áo", R.drawable.icon_quanao, this.id_user));
        list.add(new DanhMucChi("Điện thoại", R.drawable.icon_dienthoai, this.id_user));
        list.add(new DanhMucChi("Xăng xe", R.drawable.icon_xangxe, this.id_user));
        list.add(new DanhMucChi("Du lịch", R.drawable.icon_dulich, this.id_user));
        list.add(new DanhMucChi("Nhà nghỉ", R.drawable.icon_nhanghi, this.id_user));
        list.add(new DanhMucChi("Phụ phí", R.drawable.icon_phuphi, this.id_user));
        list.add(new DanhMucChi("Nhà cửa", R.drawable.icon_nhacua, this.id_user));
        list.add(new DanhMucChi("Hiếu hỉ", R.drawable.icon_hieuhi, this.id_user));
        list.add(new DanhMucChi("Cafe", R.drawable.icon_cafe, this.id_user));

        if(UserDatabase.getInstance(this).danhMucChiDAO().getList(this.id_user).size() == 0){
            UserDatabase.getInstance(this).danhMucChiDAO().insertList(list);
        }
        return;
    }

    // nhận dữ liệu sau khi nhấn nút Luu để thêm mới DoanhThu
    @Override
    public void getStatus(int a) {
        DoanhThu fdoanhThu = (DoanhThu) getSupportFragmentManager().findFragmentById(R.id.Fragment_container);
        fdoanhThu.checkClickButtonLuu(a);
    }
    // nhận dữ liệu sau khi nhấn nút Luu để thêm mới KhoanChi
    @Override
    public void getStatusChi(int a) {
        Khoanchi fkhoanChi = (Khoanchi) getSupportFragmentManager().findFragmentById(R.id.Fragment_container);
        fkhoanChi.checkClickButtonLuu(a);
    }

    @Override
    public void thongBao(int kt) {
        if(kt == 1){
            DoanhThu fdoanhThu = (DoanhThu) getSupportFragmentManager().findFragmentById(R.id.Fragment_container);
            fdoanhThu.XuLyAfterDeleteItem(kt);
        }
        else{
            Khoanchi fkhoanChi = (Khoanchi) getSupportFragmentManager().findFragmentById(R.id.Fragment_container);
            fkhoanChi.XuLyAfterDeleteItem(kt);
        }
    }

    @Override
    public void afterDangXuat(int check) {
        if(check == 1){
            Intent intent = new Intent(this, Sign_In.class);
            Bundle bundle = new Bundle();
            bundle.putInt("check", 1);
            intent.putExtras(bundle);
            startActivity(intent);
            this.finish();
        }
    }
}