package com.example.qlthuchicanhan;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.User;
import com.example.qlthuchicanhan.TruyenDuLieu.AfterDangXuat;


public class Sign_In extends AppCompatActivity {
    TextView sign_up;
    EditText username, password;
    Button BTSign_In;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        BTSign_In = (Button) findViewById(R.id.BTLSign_in);

        checkAfterLogout();

        BTSign_In.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ValidateUser();
            }
        });

        sign_up = findViewById(R.id.sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Sign_In.this, Sign_Up.class);
                startActivity(i);
            }
        });
        SaveLoginSession();
    }

    private void ValidateUser() {
        String user_name = username.getText().toString().trim();
        String pass = password.getText().toString().trim();

        if (TextUtils.isEmpty(user_name) || TextUtils.isEmpty(pass)) {
            Toast.makeText(this, "Enter input", Toast.LENGTH_SHORT).show();
            return;
        }

        User user = UserDatabase.getInstance(this).userDAO().login(user_name, pass);
        if (user == null) {
            Toast.makeText(this, "Incorrect username or password",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        DataLocalManager.setUser(user);

        Intent i = new Intent(Sign_In.this, MainActivity.class);
        startActivity(i);
    }
    private void SaveLoginSession(){
        User user = DataLocalManager.getUser();
        if(user != null){
            Intent i = new Intent(Sign_In.this, MainActivity.class);
            startActivity(i);
        }
    }
    private void checkAfterLogout(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            username.setText("");
            password.setText("");
        }
    }
}