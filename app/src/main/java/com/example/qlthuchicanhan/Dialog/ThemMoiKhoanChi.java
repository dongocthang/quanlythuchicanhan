package com.example.qlthuchicanhan.Dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.qlthuchicanhan.Adapter.DanhMucChiAdapter;
import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.KhoanChi;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.LoadDuLieuKhoanChi;
import com.example.qlthuchicanhan.TruyenDuLieu.ThemDanhMucChiMoi;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ThemMoiKhoanChi extends BottomSheetDialogFragment implements ThemDanhMucChiMoi {

    private int id_user = -1;
    private int id_danhmucchi = -1;
    ImageView img_date, img_themmoidanhmucchi;
    Spinner spinner;
    EditText edt_sotien, edt_ghichu;
    TextView tv_date;
    Button btn_huy, btn_luu;
    DanhMucChiAdapter danhMucChiAdapter;
    List<DanhMucChi> list;
    View view;
    private DatePickerDialog.OnDateSetListener setListener;
    LoadDuLieuKhoanChi data;

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior<View> bottomSheetBehavior;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        LinearLayout layout = bottomSheetDialog.findViewById(R.id.bottomSheetDialogLayout2);
        assert layout !=null;
        layout.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        return bottomSheetDialog;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_themkhoanchi,container,false);
        init();

        loadDataSpinner();

        img_themmoidanhmucchi.setOnClickListener(v -> {
            ThemMoiDanhMucChi themMoiDanhMucChi = new ThemMoiDanhMucChi();
            themMoiDanhMucChi.show(getChildFragmentManager(), "Them Moi Danh Muc Chi");
        });

        Calendar cal =  Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        img_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault_Dialog,setListener,
                        year,month,day);
                datePickerDialog.show();
            }
        });
        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
                cal.set(year, month, day);
                tv_date.setText(formatDate.format(cal.getTime()));
            }
        };

        data = (LoadDuLieuKhoanChi) getActivity();
        clickButtonLuu();
        clickButtonHuy();

        return view;
    }

    public void init(){
        img_date = view.findViewById(R.id.imgDate);
        tv_date = view.findViewById(R.id.tv_date);
        spinner  = view.findViewById(R.id.spinner);
        edt_sotien = view.findViewById(R.id.edt_sotien);
        edt_ghichu = view.findViewById(R.id.edt_ghichu);
        btn_huy  = view.findViewById(R.id.btn_huy);
        btn_luu = view.findViewById(R.id.btn_luu);
        img_themmoidanhmucchi = view.findViewById(R.id.img_themmoidanhmucchi);
        id_user = DataLocalManager.getUser().getId();
    }

    private void clickButtonLuu() {
        btn_luu.setOnClickListener(v -> {
            String sotien = edt_sotien.getText().toString().trim();
            String ngayThu = tv_date.getText().toString().trim();
            String ghiChu = edt_ghichu.getText().toString().trim();
            if(TextUtils.isEmpty(sotien) || TextUtils.isEmpty(ngayThu)){
                Toast.makeText(getContext(), "Vui lòng nhập đủ thông tin!", Toast.LENGTH_LONG).show();
                return;
            }
            else{
                KhoanChi khoanchi = new KhoanChi(this.id_danhmucchi, Integer.parseInt(sotien), ngayThu,
                        ghiChu, this.id_user);
                UserDatabase.getInstance(getContext()).khoanChiDAO().insert(khoanchi);
                getDialog().dismiss();
                Toast.makeText(getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                data.getStatusChi(1);
            }
        });
    }

    private void clickButtonHuy() {
        btn_huy.setOnClickListener(v -> {
            getDialog().dismiss();
        });
    }

    public List<DanhMucChi> getList(){
        list = new ArrayList<>();
        return list = UserDatabase.getInstance(getContext()).danhMucChiDAO().getList(this.id_user);
    }

    public void loadDataSpinner() {
        danhMucChiAdapter = new DanhMucChiAdapter(getContext(), R.layout.item_selected_danhmuc, getList());
        spinner.setAdapter(danhMucChiAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ThemMoiKhoanChi.this.id_danhmucchi = danhMucChiAdapter.getIdDanhMuc(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void themMoiDanhMucChi(DanhMucChi danhMucChi) {
        UserDatabase.getInstance(getContext()).danhMucChiDAO().insert(danhMucChi);
        loadDataSpinner();
        spinner.setSelection(spinner.getCount()-1);
        this.id_danhmucchi = danhMucChiAdapter.getIdDanhMuc(spinner.getCount()-1);
    }
}
