package com.example.qlthuchicanhan.Dialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.qlthuchicanhan.Adapter.DanhMucAdapter;
import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.Model.DoanhThu;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.LoadDuLieuDoanhThu;
import com.example.qlthuchicanhan.TruyenDuLieu.ThemDanhMucMoi;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ThemMoiDoanhThu extends BottomSheetDialogFragment implements ThemDanhMucMoi {

    private int id_user = -1;
    private int id_danhmucthu = -1;
    ImageView img_date, img_themmoidanhmucthu;
    Spinner spinner;
    EditText edt_sotien, edt_ghichu;
    TextView tv_date;
    Button btn_huy, btn_luu;
    DanhMucAdapter danhMucAdapter;
    List<DanhMucThu> list;
    View view;
    private DatePickerDialog.OnDateSetListener setListener;
    LoadDuLieuDoanhThu data;

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior<View> bottomSheetBehavior;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        LinearLayout layout = bottomSheetDialog.findViewById(R.id.bottomSheetDialogLayout);
        assert layout !=null;
        layout.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        return bottomSheetDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_themdoanhthu2,container,false);
        init();

        loadDataSpinner();

        img_themmoidanhmucthu.setOnClickListener(v -> {
            ThemMoiDanhMuc themMoiDanhMuc = new ThemMoiDanhMuc();
            themMoiDanhMuc.show(getChildFragmentManager(), "Them Moi Danh Muc");
        });

        Calendar cal =  Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        img_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_DeviceDefault_Dialog,setListener,
                        year,month,day);
                datePickerDialog.show();
            }
        });
        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
                cal.set(year, month, day);
                tv_date.setText(formatDate.format(cal.getTime()));
            }
        };

        data = (LoadDuLieuDoanhThu) getActivity();
        clickButtonLuu();
        clickButtonHuy();

        return view;
    }

    public void init(){
        img_date = view.findViewById(R.id.imgDate);
        tv_date = view.findViewById(R.id.tv_date);
        spinner  = view.findViewById(R.id.spinner);
        edt_sotien = view.findViewById(R.id.edt_sotien);
        edt_ghichu = view.findViewById(R.id.edt_ghichu);
        btn_huy  = view.findViewById(R.id.btn_huy);
        btn_luu = view.findViewById(R.id.btn_luu);
        img_themmoidanhmucthu = view.findViewById(R.id.img_themmoidanhmucthu);
        id_user = DataLocalManager.getUser().getId();
    }

    private void clickButtonLuu() {
        btn_luu.setOnClickListener(v -> {
            String sotien = edt_sotien.getText().toString().trim();
            String ngayThu = tv_date.getText().toString().trim();
            String ghiChu = edt_ghichu.getText().toString().trim();
            if(TextUtils.isEmpty(sotien) || TextUtils.isEmpty(ngayThu)){
                Toast.makeText(getContext(), "Vui lòng nhập đủ thông tin!", Toast.LENGTH_LONG).show();
                return;
            }
            else{
                if(ghiChu.isEmpty()) ghiChu = "";
                DoanhThu doanhThu = new DoanhThu(this.id_danhmucthu, Integer.parseInt(sotien), ngayThu,
                        ghiChu, this.id_user);
                UserDatabase.getInstance(getContext()).doanhThuDAO().insert(doanhThu);
                getDialog().dismiss();
                Toast.makeText(getContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                data.getStatus(1);
            }
        });
    }

    private void clickButtonHuy() {
        btn_huy.setOnClickListener(v -> {
            getDialog().dismiss();
        });
    }

    public List<DanhMucThu> getList(){
        list = new ArrayList<>();
        return list = UserDatabase.getInstance(getContext()).danhMucThuDAO().getList(this.id_user);
    }

    public void loadDataSpinner() {
        danhMucAdapter = new DanhMucAdapter(getContext(), R.layout.item_selected_danhmuc, getList());
        spinner.setAdapter(danhMucAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ThemMoiDoanhThu.this.id_danhmucthu = danhMucAdapter.getIdDanhMuc(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void themMoiDanhMucThu(DanhMucThu danhMucThu) {
        UserDatabase.getInstance(getContext()).danhMucThuDAO().insert(danhMucThu);
        loadDataSpinner();
        spinner.setSelection(spinner.getCount()-1);
        this.id_danhmucthu = danhMucAdapter.getIdDanhMuc(spinner.getCount()-1);
    }
}
