package com.example.qlthuchicanhan.Dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.ThemDanhMucChiMoi;
import com.example.qlthuchicanhan.TruyenDuLieu.ThemDanhMucMoi;

public class ThemMoiDanhMucChi extends DialogFragment {

    private int id_user = -1;
    Button btn_THEM, btn_HUY;
    EditText edt_nhapdanhmuc;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_themdanhmucmoi, container, false);
        btn_THEM = view.findViewById(R.id.btn_THEM);
        btn_HUY  = view.findViewById(R.id.btn_HUY);
        edt_nhapdanhmuc = view.findViewById(R.id.edt_nhapdanhmuc);

        id_user = DataLocalManager.getUser().getId();

        clickButtonTHEM();
        clickButtonHUY();

        return view;
    }

    private void clickButtonTHEM() {
        btn_THEM.setOnClickListener(v -> {
            if(TextUtils.isEmpty(edt_nhapdanhmuc.getText().toString().trim())){
                Toast.makeText(getContext(), "bạn chưa nhập danh mục!",
                        Toast.LENGTH_SHORT).show();
                return;
            }
            else{
                String name = edt_nhapdanhmuc.getText().toString().trim();
                DanhMucChi danhMucChi = new DanhMucChi(name, R.drawable.icon_new, this.id_user);
                // truyền data về cha và thêm vào csdl
                ThemDanhMucChiMoi data = (ThemDanhMucChiMoi) getParentFragment();
                data.themMoiDanhMucChi(danhMucChi);
                Toast.makeText(getContext(), "Đã thêm 1 danh mục chi mới", Toast.LENGTH_SHORT).show();
                getDialog().dismiss();
            }
        });
    }

    private void clickButtonHUY() {
        btn_HUY.setOnClickListener(v -> {
            getDialog().dismiss();
        });
    }
}
