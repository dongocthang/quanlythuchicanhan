package com.example.qlthuchicanhan.Dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.DuLieuThangNam;

import java.util.ArrayList;
import java.util.Calendar;

public class ChonNam extends DialogFragment {
    ImageView img_tiennam, img_luinam;
    TextView tv_nam;
    Spinner spinner_chonthang;
    Button btn_ok, btn_boqua;
    View view;
    DuLieuThangNam getThoiGian;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_calender, container, false);
        init();

        Calendar calendar = Calendar.getInstance();
        int nam = calendar.get(Calendar.YEAR);
        int thang = calendar.get(Calendar.MONTH);
        tv_nam.setText(String.valueOf(nam));

        // click ImageView Tiến
        img_tiennam.setOnClickListener(v -> {
            int getThangTV = Integer.parseInt(tv_nam.getText().toString());
            tv_nam.setText(String.valueOf(getThangTV + 1));
        });
        // click ImageView Lùi
        img_luinam.setOnClickListener(v -> {
            int getThangTV = Integer.parseInt(tv_nam.getText().toString());
            tv_nam.setText(String.valueOf(getThangTV - 1));
        });

        ArrayList<String> listThang = new ArrayList<String>();
        listThang.add("1");  listThang.add("2");
        listThang.add("3");  listThang.add("4");
        listThang.add("5");  listThang.add("6");
        listThang.add("7");  listThang.add("8");
        listThang.add("9");  listThang.add("10");
        listThang.add("11"); listThang.add("12");

        ArrayAdapter adapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item,
                listThang);
        spinner_chonthang.setAdapter(adapter);
        spinner_chonthang.setSelection(thang);

        clickButtonOk();
        clickButtonBoQua();

        return view;
    }

    private void init(){
        img_tiennam = view.findViewById(R.id.img_tiennam);
        img_luinam = view.findViewById(R.id.img_luinam);
        tv_nam = view.findViewById(R.id.tv_nam);
        spinner_chonthang = view.findViewById(R.id.spinner_chonthang);
        btn_ok = view.findViewById(R.id.btn_ok);
        btn_boqua = view.findViewById(R.id.btn_boqua);
    }

    private void clickButtonOk(){
        btn_ok.setOnClickListener(v -> {
            getThoiGian = (DuLieuThangNam) getParentFragment();
            int a = Integer.parseInt((String) spinner_chonthang.getSelectedItem());
            int b = Integer.parseInt(tv_nam.getText().toString());
            getThoiGian.getThangNam(a, b);
            getDialog().dismiss();
        });
    }

    private void clickButtonBoQua() {
        btn_boqua.setOnClickListener(v -> {
            getDialog().dismiss();
        });
    }
}
