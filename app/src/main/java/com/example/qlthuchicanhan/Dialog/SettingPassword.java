package com.example.qlthuchicanhan.Dialog;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.User;
import com.example.qlthuchicanhan.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SettingPassword extends BottomSheetDialogFragment {

    private EditText editTextCurrentPassword;
    private EditText editTextNewPassword;
    private EditText editTextNewPasswordConfirm;
    private Button btnUpdate;
    private TextView textViewSave;
    private TextView textViewClose;

    private BottomSheetDialog bottomSheetDialog;
    private BottomSheetBehavior<View> bottomSheetBehavior;

    private View view;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bottomSheetBehavior = BottomSheetBehavior.from((View) view.getParent());
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        LinearLayout layout = bottomSheetDialog.findViewById(R.id.bottomSheetDialogSettingPassword);
        assert layout !=null;
        layout.setMinimumHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        return bottomSheetDialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.setting_password_layout,container,false);
        init();

        textViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        textViewSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdatePassword();
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdatePassword();
            }
        });


        return view;
    }

    private void UpdatePassword() {
        String currentPassword = editTextCurrentPassword.getText().toString().trim();
        String newPassword = editTextNewPassword.getText().toString().trim();
        String newPasswordConfirm = editTextNewPasswordConfirm.getText().toString().trim();

        User user = DataLocalManager.getUser();
        if(currentPassword.isEmpty() || newPassword.isEmpty() || newPasswordConfirm.isEmpty()){
            Toast.makeText(getContext(), "Vui lòng nhập đủ thông tin", Toast.LENGTH_SHORT).show();
        }
        else{
            if (currentPassword.equals(user.getPassword()) == false){
                Toast.makeText(getContext(), "Mật khẩu cũ không đúng", Toast.LENGTH_SHORT).show();
            }
            else {
                if(newPassword.equals(newPasswordConfirm)){
                    user.setPassword(newPassword);
                    UserDatabase.getInstance(getContext()).userDAO().UpdatePassword(user);
                    new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                            .setTitleText("Thông báo!")
                            .setContentText("Đổi mật khẩu thành công!")
                            .setConfirmText("OK").show();
                    getDialog().dismiss();
                }
                else {
                    Toast.makeText(getContext(), "Mật khẩu mới không khớp!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void init() {
        editTextCurrentPassword = view.findViewById(R.id.currentPassword);
        editTextNewPassword = view.findViewById(R.id.newPassword);
        editTextNewPasswordConfirm = view.findViewById(R.id.newPasswordConfirm);
        btnUpdate = view.findViewById(R.id.btnUpdatePass);
        textViewSave = view.findViewById(R.id.textViewSave);
        textViewClose = view.findViewById(R.id.textViewClose);

        editTextCurrentPassword.setText("");
        editTextNewPassword.setText("");
        editTextNewPasswordConfirm.setText("");
    }
}
