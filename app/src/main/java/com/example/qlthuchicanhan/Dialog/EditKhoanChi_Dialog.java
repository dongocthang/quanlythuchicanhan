package com.example.qlthuchicanhan.Dialog;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.qlthuchicanhan.Adapter.DanhMucChiAdapter;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.ItemKhoanChi;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.DataAfterDeleteOrHuyItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EditKhoanChi_Dialog extends DialogFragment {
    View view;
    ImageView img_date, img_themmoidanhmucchi;
    Spinner spinner;
    EditText edt_sotien, edt_ghichu;
    TextView tv_date;
    Button btn_huy, btn_luu;

    DanhMucChiAdapter danhMucChiAdapter;
    List<DanhMucChi> list;
    DataAfterDeleteOrHuyItem data;
    private DatePickerDialog.OnDateSetListener setListener;

    private int id_user = -1;
    private int id_danhmucchi = -1;
    private int id = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_themkhoanchi,container,false);
        init();

        Bundle bundle = getArguments();
        ItemKhoanChi item = (ItemKhoanChi) bundle.getSerializable("item");

        this.id_user = item.getId_user();
        this.id = item.getId();
        // set dữ liệu tương ứng
        edt_sotien.setText(String.valueOf(item.getSotien()));
        tv_date.setText(item.getNgaychi());
        edt_ghichu.setText(item.getGhichu());

        this.id_danhmucchi = item.getId_danhmucchi();
        loadDataSpinner();
        // gán ảnh và tên danh mục cần chỉnh cho spinner
        for(int i = 0; i < spinner.getCount(); i++){
            if(item.getId_danhmucchi() == danhMucChiAdapter.getIdDanhMuc(i)){
                spinner.setSelection(i);
                break;
            }
        }

        Calendar cal =  Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        img_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        android.R.style.Theme_DeviceDefault_Dialog,setListener, year,month,day);
                datePickerDialog.show();
            }
        });
        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
                cal.set(year, month, day);
                tv_date.setText(formatDate.format(cal.getTime()));
            }
        };

        data = (DataAfterDeleteOrHuyItem) getActivity();
        clickButtonLuu();
        clickButtonHuy();

        // chạm vào ngoài dialog thì không được hủy
        getDialog().setCanceledOnTouchOutside(false);
        return view;
    }

    public void init(){
        img_date = view.findViewById(R.id.imgDate);
        tv_date = view.findViewById(R.id.tv_date);
        spinner  = view.findViewById(R.id.spinner);
        edt_sotien = view.findViewById(R.id.edt_sotien);
        edt_ghichu = view.findViewById(R.id.edt_ghichu);
        btn_huy  = view.findViewById(R.id.btn_huy);
        btn_luu = view.findViewById(R.id.btn_luu);
        img_themmoidanhmucchi = view.findViewById(R.id.img_themmoidanhmucchi);
    }

    public List<DanhMucChi> getList(){
        list = new ArrayList<>();
        return list = UserDatabase.getInstance(getContext()).danhMucChiDAO().getList(this.id_user);
    }

    private void loadDataSpinner() {
        danhMucChiAdapter = new DanhMucChiAdapter(getContext(), R.layout.item_selected_danhmuc, getList());
        spinner.setAdapter(danhMucChiAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EditKhoanChi_Dialog.this.id_danhmucchi = danhMucChiAdapter.getIdDanhMuc(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void clickButtonLuu(){
        btn_luu.setOnClickListener(v -> {
            String sotien = edt_sotien.getText().toString().trim();
            String ngayChi = tv_date.getText().toString().trim();
            String ghiChu = edt_ghichu.getText().toString().trim();
            if(TextUtils.isEmpty(sotien) || TextUtils.isEmpty(ngayChi)){
                Toast.makeText(getContext(), "Vui lòng nhập đủ thông tin!", Toast.LENGTH_LONG).show();
                return;
            }
            else{
                UserDatabase.getInstance(getContext()).khoanChiDAO().update(id_danhmucchi, Long.parseLong(sotien),
                        ngayChi, ghiChu, id_user, id);
                getDialog().dismiss();
                Toast.makeText(getContext(), "Sửa thành công", Toast.LENGTH_SHORT).show();
                data.thongBao(2);
            }
        });
    }

    public void clickButtonHuy(){
        btn_huy.setOnClickListener(v -> {
            getDialog().dismiss();
            data.thongBao(2);
        });
    }
}
