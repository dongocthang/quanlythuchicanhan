package com.example.qlthuchicanhan.Dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.qlthuchicanhan.Adapter.DanhMucAdapter;
import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.MainActivity;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.R;

import java.util.List;

public class DanhMuc extends DialogFragment {

    private int id_user = -1;
    private ListView lvDanhMuc;
    List<DanhMucThu> list;
    DanhMucAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        id_user = DataLocalManager.getUser().getId();
        Toast.makeText(getContext(), "thang " + id_user, Toast.LENGTH_SHORT).show();

        View view = inflater.inflate(R.layout.dialog_fragment_themdanhmuc, container, false);

        lvDanhMuc = view.findViewById(R.id.lv_danhmuc);
        loadAllDanhMucThu();
        adapter = new DanhMucAdapter(getContext(), R.layout.item_danhmuc, list);
        adapter.notifyDataSetChanged();
        lvDanhMuc.setAdapter(adapter);

        return view;
    }

    public void loadAllDanhMucThu(){
        list = UserDatabase.getInstance(getContext()).danhMucThuDAO().getList(this.id_user);
    }
}
