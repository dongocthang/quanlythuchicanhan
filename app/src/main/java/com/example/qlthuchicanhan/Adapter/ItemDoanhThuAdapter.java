package com.example.qlthuchicanhan.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Dialog.Edit_Dialog;
import com.example.qlthuchicanhan.Model.ItemDoanhThu;
import com.example.qlthuchicanhan.R;

import java.io.Serializable;
import java.util.List;

public class ItemDoanhThuAdapter extends RecyclerView.Adapter<ItemDoanhThuAdapter.ItemDoanhThuViewHolder>{

    private Context context;
    private List<ItemDoanhThu> itemDoanhThuList;

    public ItemDoanhThuAdapter(Context context){
        this.context = context;
    }

    public void setData(List<ItemDoanhThu> itemDoanhThuList){
        this.itemDoanhThuList = itemDoanhThuList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemDoanhThuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doanhthu, parent,
                false);
        return new ItemDoanhThuViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ItemDoanhThuViewHolder holder, int position) {
        ItemDoanhThu itemDoanhThu = itemDoanhThuList.get(position);
        if(itemDoanhThu == null){
            return;
        }

        holder.img_anh.setImageResource(itemDoanhThu.getImage());
        holder.tv_tendanhmuc.setText(itemDoanhThu.getName());
        holder.tv_ghichu.setText(itemDoanhThu.getGhichu());
        holder.tv_tien.setText("+" + itemDoanhThu.getSotien());
    }

    @Override
    public int getItemCount() {
        if(itemDoanhThuList != null){
            return itemDoanhThuList.size();
        }
        return 0;
    }

    public void deleteItem(int position) {
        ItemDoanhThu item = itemDoanhThuList.get(position);
        UserDatabase.getInstance(context).doanhThuDAO().delete(item.getId());
        itemDoanhThuList.remove(position);
        notifyItemRemoved(position);
    }

    public void editItem(int position){
        ItemDoanhThu item = itemDoanhThuList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", item);
        FragmentActivity activity = (FragmentActivity)(context);
        FragmentManager fm = activity.getSupportFragmentManager();
        Edit_Dialog dialog = new Edit_Dialog();
        dialog.setArguments(bundle);
        dialog.show(fm, "Edit_Dialog");
    }

    public Context getContext(){
        return context;
    }


    public class ItemDoanhThuViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_anh;
        private TextView tv_tendanhmuc, tv_ghichu, tv_tien;

        public ItemDoanhThuViewHolder(@NonNull View itemView) {
            super(itemView);

            img_anh       = itemView.findViewById(R.id.imageDanhMuc);
            tv_tendanhmuc = itemView.findViewById(R.id.textViewDanhMuc);
            tv_ghichu     = itemView.findViewById(R.id.description);
            tv_tien       = itemView.findViewById(R.id.textViewPrice);
        }
    }
}
