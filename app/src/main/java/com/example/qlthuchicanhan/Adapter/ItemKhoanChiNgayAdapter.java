package com.example.qlthuchicanhan.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlthuchicanhan.Model.ItemDoanhThuNgay;
import com.example.qlthuchicanhan.Model.ItemKhoanChiNgay;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.RecyclerItemTouchHelper;
import com.example.qlthuchicanhan.RecyclerItemTouchHelperKhoanChi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ItemKhoanChiNgayAdapter extends RecyclerView.Adapter<ItemKhoanChiNgayAdapter.ItemKhoanChiNgayViewHolder>{

    private Context context;
    private List<ItemKhoanChiNgay> list;
    SimpleDateFormat formatDate = new SimpleDateFormat("EE dd MMM yyyy", Locale.US);
    SimpleDateFormat inputDate  = new SimpleDateFormat("dd/M/yyyy", Locale.US);
    Date date = null;
    String outputDateString = null;

    public ItemKhoanChiNgayAdapter(Context context){
        this.context = context;
    }

    public void setData(List<ItemKhoanChiNgay> list){
        this.list = list;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ItemKhoanChiNgayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_khoanchi_ngay,
                parent,false);
        return new ItemKhoanChiNgayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemKhoanChiNgayViewHolder holder, int position) {
        ItemKhoanChiNgay item = list.get(position);
        if(item == null){
            return;
        }

        try {
            date = inputDate.parse(item.getDate());
            outputDateString = formatDate.format(date);
            String[] mang = outputDateString.split(" ");
            String thu = mang[0];
            String ngay = mang[1];
            String thang = mang[2];
            String nam = mang[3];
            String tienngay = String.valueOf(item.getTien());

            // gán giá trị cho các View header
            holder.tv_thu.setText(thu);
            holder.tv_ngay.setText(ngay);
            holder.tv_thang.setText(thang);
            holder.tv_nam.setText(nam);
            holder.tv_tienNgay.setText(tienngay + "đ");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // gán giá trị cho RecylerView Item
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false);
        holder.rcv_listItemKhoanChi.setLayoutManager(linearLayoutManager);

        ItemKhoanChiAdapter itemKhoanChiAdapter = new ItemKhoanChiAdapter(context);
        itemKhoanChiAdapter.setData(item.getItemKhoanChis());
        holder.rcv_listItemKhoanChi.setAdapter(itemKhoanChiAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(
                new RecyclerItemTouchHelperKhoanChi(itemKhoanChiAdapter));
        itemTouchHelper.attachToRecyclerView(holder.rcv_listItemKhoanChi);
    }

    @Override
    public int getItemCount() {
        if(list != null){
            return list.size();
        }
        return 0;
    }

    public class ItemKhoanChiNgayViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_thu, tv_ngay, tv_thang, tv_nam, tv_tienNgay;
        private RecyclerView rcv_listItemKhoanChi;

        public ItemKhoanChiNgayViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_thu               = itemView.findViewById(R.id.textViewDay);
            tv_ngay              = itemView.findViewById(R.id.textViewDate);
            tv_thang             = itemView.findViewById(R.id.textViewMonth);
            tv_nam               = itemView.findViewById(R.id.textViewYear);
            tv_tienNgay          = itemView.findViewById(R.id.textViewTotal);
            rcv_listItemKhoanChi = itemView.findViewById(R.id.list_item_indatetime);
        }
    }
}
