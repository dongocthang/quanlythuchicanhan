package com.example.qlthuchicanhan.Adapter;


import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.qlthuchicanhan.Fragment.Nam;
import com.example.qlthuchicanhan.Fragment.Tatca;
import com.example.qlthuchicanhan.Fragment.Thang;
import com.example.qlthuchicanhan.Menu.Thongke;
import com.example.qlthuchicanhan.R;

public class ViewPagerAdapter extends FragmentStateAdapter {

    private Bundle bundle;

    //    private final Fragment[] mFragments = new Fragment[] {//Initialize fragments views
////Fragment views are initialized like any other fragment (Extending Fragment)
//            new Tatca(),//First fragment to be displayed within the pager tab number 1
//            new Thang(),
//            new Nam(),
//    };
    public final String[] mFragmentNames = new String[] {//Tabs names array
            "Tất cả",
            "Tháng",
            "Năm"
    };

    public ViewPagerAdapter(FragmentActivity fa){//Pager constructor receives Activity instance
        super(fa);
    }

    @Override
    public int getItemCount() {
        return 3;//Number of fragments displayed
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 0:
                Tatca tatca = new Tatca();
                return tatca;
            case 1:
                Thang thang = new Thang();
                return thang;
            case 2:
                Nam nam = new Nam();
                return nam;
            default:
                Tatca tatca1 = new Tatca();
                return tatca1;
        }
    }
}