package com.example.qlthuchicanhan.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.R;

import java.util.List;

public class DanhMucChiAdapter extends ArrayAdapter<DanhMucChi> {

    public DanhMucChiAdapter(@NonNull Context context, int resource, @NonNull List<DanhMucChi> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_danhmuc,
                parent, false);
        ImageView img_selecteddanhmucmoi = convertView.findViewById(R.id.img_selecteddanhmucmoi);
        TextView tv_selecteddanhmuc      = convertView.findViewById(R.id.tv_selectedtendanhmuc);

        DanhMucChi danhMucChi = this.getItem(position);
        if(danhMucChi != null){
            img_selecteddanhmucmoi.setImageResource(danhMucChi.getImage());
            tv_selecteddanhmuc.setText(danhMucChi.getName());
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_danhmuc, parent, false);
        ImageView img_danhmucmoi = convertView.findViewById(R.id.img_danhmuc);
        TextView tv_danhmuc      = convertView.findViewById(R.id.tv_tendanhmuc);

        DanhMucChi danhMucChi = this.getItem(position);
        if(danhMucChi != null){
            img_danhmucmoi.setImageResource(danhMucChi.getImage());
            tv_danhmuc.setText(danhMucChi.getName());
        }
        return convertView;
    }

    public int getIdDanhMuc(int position){
        DanhMucChi danhMucChi = this.getItem(position);
        return danhMucChi.getId();
    }
}
