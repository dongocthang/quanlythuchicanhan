package com.example.qlthuchicanhan.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.R;

import java.util.List;

public class DanhMucAdapter extends ArrayAdapter<DanhMucThu> {

    public DanhMucAdapter(@NonNull Context context, int resource, @NonNull List<DanhMucThu> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_danhmuc,
                parent, false);
        ImageView img_selecteddanhmucmoi = convertView.findViewById(R.id.img_selecteddanhmucmoi);
        TextView tv_selecteddanhmuc      = convertView.findViewById(R.id.tv_selectedtendanhmuc);

        DanhMucThu danhMucThu = this.getItem(position);
        if(danhMucThu != null){
            img_selecteddanhmucmoi.setImageResource(danhMucThu.getImage());
            tv_selecteddanhmuc.setText(danhMucThu.getName());
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_danhmuc, parent, false);
        ImageView img_danhmucmoi = convertView.findViewById(R.id.img_danhmuc);
        TextView tv_danhmuc      = convertView.findViewById(R.id.tv_tendanhmuc);

        DanhMucThu danhMucThu = this.getItem(position);
        if(danhMucThu != null){
            img_danhmucmoi.setImageResource(danhMucThu.getImage());
            tv_danhmuc.setText(danhMucThu.getName());
        }
        return convertView;
    }

    public int getIdDanhMuc(int position){
        DanhMucThu danhMucThu = this.getItem(position);
        return danhMucThu.getId();
    }
}
