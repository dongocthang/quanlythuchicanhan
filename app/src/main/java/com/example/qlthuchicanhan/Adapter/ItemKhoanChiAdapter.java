package com.example.qlthuchicanhan.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Dialog.EditKhoanChi_Dialog;
import com.example.qlthuchicanhan.Model.ItemKhoanChi;
import com.example.qlthuchicanhan.R;

import java.util.List;

public class ItemKhoanChiAdapter extends RecyclerView.Adapter<ItemKhoanChiAdapter.ItemKhoanChiViewHolder>{

    private Context context;
    private List<ItemKhoanChi> itemKhoanChiList;

    public ItemKhoanChiAdapter(Context context){
        this.context = context;
    }

    public void setData(List<ItemKhoanChi> itemKhoanChiList){
        this.itemKhoanChiList = itemKhoanChiList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ItemKhoanChiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doanhthu, parent,
                false);
        return new ItemKhoanChiViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ItemKhoanChiViewHolder holder, int position) {
        ItemKhoanChi itemKhoanChi = itemKhoanChiList.get(position);
        if(itemKhoanChi == null){
            return;
        }

        holder.img_anh.setImageResource(itemKhoanChi.getImage());
        holder.tv_tendanhmuc.setText(itemKhoanChi.getName());
        holder.tv_ghichu.setText(itemKhoanChi.getGhichu());
        holder.tv_tien.setTextColor(Color.RED);
        holder.tv_tien.setText("-" + itemKhoanChi.getSotien());
    }

    @Override
    public int getItemCount() {
        if(itemKhoanChiList != null){
            return itemKhoanChiList.size();
        }
        return 0;
    }

    public void deleteItem(int position) {
        ItemKhoanChi item = itemKhoanChiList.get(position);
        UserDatabase.getInstance(context).khoanChiDAO().delete(item.getId());
        itemKhoanChiList.remove(position);
        notifyItemRemoved(position);
    }

    public void editItem(int position){
        ItemKhoanChi item = itemKhoanChiList.get(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("item", item);
        FragmentActivity activity = (FragmentActivity)(context);
        FragmentManager fm = activity.getSupportFragmentManager();
        EditKhoanChi_Dialog dialog = new EditKhoanChi_Dialog();
        dialog.setArguments(bundle);
        dialog.show(fm, "EditKhoanChi_Dialog");
    }

    public Context getContext(){
        return context;
    }

    public class ItemKhoanChiViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_anh;
        private TextView tv_tendanhmuc, tv_ghichu, tv_tien;

        public ItemKhoanChiViewHolder(@NonNull View itemView) {
            super(itemView);

            img_anh       = itemView.findViewById(R.id.imageDanhMuc);
            tv_tendanhmuc = itemView.findViewById(R.id.textViewDanhMuc);
            tv_ghichu     = itemView.findViewById(R.id.description);
            tv_tien       = itemView.findViewById(R.id.textViewPrice);
        }
    }
}
