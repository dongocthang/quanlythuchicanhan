package com.example.qlthuchicanhan.Database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.AutoMigration;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.qlthuchicanhan.DAO.DanhMucChiDAO;
import com.example.qlthuchicanhan.DAO.DanhMucThuDAO;
import com.example.qlthuchicanhan.DAO.DoanhThuDAO;
import com.example.qlthuchicanhan.DAO.KhoanChiDAO;
import com.example.qlthuchicanhan.DAO.UserDAO;
import com.example.qlthuchicanhan.Model.Converters;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.Model.DoanhThu;
import com.example.qlthuchicanhan.Model.KhoanChi;
import com.example.qlthuchicanhan.Model.User;

//.fallbackToDestructiveMigration()
@Database(entities = {User.class, DanhMucThu.class, DanhMucChi.class, DoanhThu.class, KhoanChi.class},version = 1)
public abstract class UserDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "database";
    private static UserDatabase instance;
    public static synchronized UserDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), UserDatabase.class,
                    DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

    public abstract UserDAO userDAO();
    public abstract DanhMucThuDAO danhMucThuDAO();
    public abstract DanhMucChiDAO danhMucChiDAO();
    public abstract DoanhThuDAO doanhThuDAO();
    public abstract KhoanChiDAO khoanChiDAO();
}
