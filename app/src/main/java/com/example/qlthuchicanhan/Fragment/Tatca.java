package com.example.qlthuchicanhan.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.DoanhThu;
import com.example.qlthuchicanhan.Model.KhoanChi;
import com.example.qlthuchicanhan.Model.User;
import com.example.qlthuchicanhan.PieChart;
import com.example.qlthuchicanhan.R;

import java.util.ArrayList;
import java.util.List;


public class Tatca extends Fragment {

    View view;
    TextView tvTongThu, tvChiTieu, tvCanDoi;
    ImageView imgBieuDo;
    private int id_user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tatca, container, false);

        init();

        id_user = DataLocalManager.getUser().getId();
        tinhToan();

        clickImageViewBieuDo();
        return view;
    }

    public void init(){
        tvTongThu = view.findViewById(R.id.tv_tongThu);
        tvChiTieu = view.findViewById(R.id.tv_chiTieu);
        tvCanDoi  = view.findViewById(R.id.tv_canDoi);
        imgBieuDo = view.findViewById(R.id.img_moBieuDo);
    }

    public void tinhToan(){
        List<DoanhThu> doanhThuList = UserDatabase.getInstance(getContext()).doanhThuDAO()
                .getList(this.id_user);

        long tongThu = 0;
        for (DoanhThu dt : doanhThuList) {
            tongThu += dt.getSotien();
        }
        tvTongThu.setText(tongThu + " vnđ");

        List<KhoanChi> khoanChiList = UserDatabase.getInstance(getContext()).khoanChiDAO()
                .getList(this.id_user);
        long tongChi = 0;
        for (KhoanChi kc : khoanChiList) {
            tongChi += kc.getSotien();
        }
        tvChiTieu.setText(tongChi + " vnđ");
        tvCanDoi.setText((tongThu - tongChi) + " vnđ");
    }

    private void clickImageViewBieuDo(){
        imgBieuDo.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), PieChart.class);
            startActivity(i);
        });
    }
}