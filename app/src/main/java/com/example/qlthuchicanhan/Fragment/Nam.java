package com.example.qlthuchicanhan.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qlthuchicanhan.DataLocal.DataLocalManager;
import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.DoanhThu;
import com.example.qlthuchicanhan.Model.KhoanChi;
import com.example.qlthuchicanhan.PieChart;
import com.example.qlthuchicanhan.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Nam extends Fragment {

    View view;
    Spinner spnNam;
    TextView tvTongThu, tvChiTieu, tvCanDoi;
    ImageView imgBieuDo;
    private int id_user;
    private int year;
    private ArrayList<Integer> listNam;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_nam, container, false);

        id_user = DataLocalManager.getUser().getId();

        init();
        loadDataSpinner();
        Calendar calendar = Calendar.getInstance();
        this.year   = calendar.get(Calendar.YEAR);
        soSanh(this.listNam);
        tinhToan();

        //mỗi lần chọn lại năm sẽ tính lại dữ liệu
        spnNam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Nam.this.year = (int) spnNam.getSelectedItem();
                tinhToan();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        clickImageViewBieuDo();
        return view;
    }

    private void init() {
        spnNam    = view.findViewById(R.id.spn_nam);
        tvTongThu = view.findViewById(R.id.tv_tongThu);
        tvChiTieu = view.findViewById(R.id.tv_chiTieu);
        tvCanDoi  = view.findViewById(R.id.tv_canDoi);
        imgBieuDo = view.findViewById(R.id.img_moBieuDo);
    }

    private void loadDataSpinner(){
        this.listNam = new ArrayList<>();
        for(int i = 2020; i < 2035; i++){
            this.listNam.add(i);
        }
        ArrayAdapter adapterNam = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item,
                this.listNam);
        spnNam.setAdapter(adapterNam);
    }

    public void soSanh(ArrayList<Integer> listNam){
        for(int i = 0; i < listNam.size(); i++){
            if(listNam.get(i) == this.year) {
                spnNam.setSelection(i);
            }
        }
    }

    private void tinhToan() {
        List<DoanhThu> listThu = UserDatabase.getInstance(getContext()).doanhThuDAO().getList(this.id_user);
        long tongThu = 0;
        for (DoanhThu dt : listThu) {
            String ngayThu = dt.getNgaythu();
            int nam   = Integer.parseInt(ngayThu.substring(6));
            if(nam == year){
                tongThu += dt.getSotien();
            }
        }
        tvTongThu.setText(tongThu + " vnđ");

        List<KhoanChi> listChi = UserDatabase.getInstance(getContext()).khoanChiDAO().getList(this.id_user);
        long tongChi = 0;
        for (KhoanChi dt : listChi) {
            String ngayChi = dt.getNgaychi();
            int nam   = Integer.parseInt(ngayChi.substring(6));
            if(nam == year){
                tongChi += dt.getSotien();
            }
        }
        tvChiTieu.setText(tongChi + " vnđ");

        tvCanDoi.setText((tongThu - tongChi) + " vnđ");
    }

    private void clickImageViewBieuDo(){
        imgBieuDo.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), PieChart.class);
            startActivity(i);
        });
    }

}