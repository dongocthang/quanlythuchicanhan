package com.example.qlthuchicanhan.Fragment;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.qlthuchicanhan.Database.UserDatabase;
import com.example.qlthuchicanhan.Model.DanhMucChi;
import com.example.qlthuchicanhan.Model.DanhMucThu;
import com.example.qlthuchicanhan.Model.ItemDoanhThu;
import com.example.qlthuchicanhan.Model.ItemKhoanChi;
import com.example.qlthuchicanhan.R;
import com.example.qlthuchicanhan.TruyenDuLieu.DuLieuTongTien;

import org.eazegraph.lib.models.PieModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BieuDo extends Fragment {

    View view;
    org.eazegraph.lib.charts.PieChart pieChart;
    TextView tvTenKhac, tvTenA, tvTenB, tvTenC, tvTenD, tvTienKhac, tvTienA, tvTienB, tvTienC,
            tvTienD, tvPhanTramA, tvPhanTramB, tvPhanTramC, tvPhanTramD, tvPhanTramKhac;
    LinearLayout lnChuThich1, lnChuThich2, lnChuThich3, lnChuThich4, lnChuThich5;
    private int id_user;
    private int month, year;
    private long tongTien = 0;
    private int status;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_bieudo, container, false);
        Bundle bundle = getArguments();
        this.id_user  = bundle.getInt("id_user");
        this.status   = bundle.getInt("status");
        this.month    = bundle.getInt("thang");
        this.year     = bundle.getInt("nam");
        init();

        anViewChuThichBanDau();
        luaChonHienThi();
        DuLieuTongTien data = (DuLieuTongTien) getActivity();
        data.getTongTien(this.tongTien);

        return view;
    }

    private void init(){
        pieChart = view.findViewById(R.id.piechart);

        tvTenKhac = view.findViewById(R.id.tv_tenChuThichKhac);
        tvTenA = view.findViewById(R.id.tv_tenChuThichA);
        tvTenB = view.findViewById(R.id.tv_tenChuThichB);
        tvTenC = view.findViewById(R.id.tv_tenChuThichC);
        tvTenD = view.findViewById(R.id.tv_tenChuThichD);

        tvTienKhac = view.findViewById(R.id.tv_tienKhac);
        tvTienA = view.findViewById(R.id.tv_tienA);
        tvTienB = view.findViewById(R.id.tv_tienB);
        tvTienC = view.findViewById(R.id.tv_tienC);
        tvTienD = view.findViewById(R.id.tv_tienD);

        tvPhanTramKhac = view.findViewById(R.id.tv_PhanTramKhac);
        tvPhanTramA = view.findViewById(R.id.tv_PhanTramA);
        tvPhanTramB = view.findViewById(R.id.tv_PhanTramB);
        tvPhanTramC = view.findViewById(R.id.tv_PhanTramC);
        tvPhanTramD = view.findViewById(R.id.tv_PhanTramD);

        lnChuThich1 = view.findViewById(R.id.ln_chuThich1);
        lnChuThich2 = view.findViewById(R.id.ln_chuThich2);
        lnChuThich3 = view.findViewById(R.id.ln_chuThich3);
        lnChuThich4 = view.findViewById(R.id.ln_chuThich4);
        lnChuThich5 = view.findViewById(R.id.ln_chuThich5);
    }

    // lựa chọn hiện thị: 1 -> chi, 2 -> thu
    private void luaChonHienThi(){
        if(this.status == 1){
            xuLyChi();
        }
        else{
            xuLyThu();
        }
    }

    public void setDataChartChi(List<Long> listTongTien, List<Integer> list2) {
        for (long tong : listTongTien) {
            tongTien += tong;
        }

        Map<Integer, Long> map = new HashMap<Integer, Long>();
        for(int i = 0; i < list2.size(); i++){
            map.put(list2.get(i), listTongTien.get(i));
        }
        // sắp xếp giảm dần theo số tiền
        List<Map.Entry<Integer, Long>> list = new ArrayList<Map.Entry<Integer, Long>>();
        list.addAll(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Long>>() {
            @Override
            public int compare(Map.Entry<Integer, Long> o1, Map.Entry<Integer, Long> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        if(list2.size() < 5){
            int kt = 0;
            List<String> mau = new ArrayList<>();
            mau.add("#FFA726");
            mau.add("#66BB6A");
            mau.add("#EF5350");
            mau.add("#29B6F6");
            mau.add("#9F9F9E");
            for(int i = 0; i < listTongTien.size(); i++){
                List<DanhMucChi> danhMucChiList = UserDatabase.getInstance(getContext())
                        .danhMucChiDAO().danhSachTheoId(list2.get(i));
                Double a = ((double)list.get(i).getValue() / (double) tongTien) * 100;
                String tien = String.format("%.2f", a);
                if(i == 0){
                    tvPhanTramA.setText(tien + "%");
                    tvTenA.setText(danhMucChiList.get(0).getName());
                    tvTienA.setText(list.get(0).getValue() + " đ");
                }
                else if(i == 1){
                    tvPhanTramB.setText(tien + "%");
                    tvTenB.setText(danhMucChiList.get(0).getName());
                    tvTienB.setText(list.get(1).getValue() + " đ");
                }
                else if(i == 2){
                    tvPhanTramC.setText(tien + "%");
                    tvTenC.setText(danhMucChiList.get(0).getName());
                    tvTienC.setText(list.get(2).getValue() + " đ");
                }
                else if(i == 3){
                    tvPhanTramD.setText(tien + "%");
                    tvTenD.setText(danhMucChiList.get(0).getName());
                    tvTienD.setText(list.get(3).getValue() + " đ");
                }
                pieChart.addPieSlice(
                        new PieModel(
                                danhMucChiList.get(0).getName(),
                                (float) Double.parseDouble(tien),
                                Color.parseColor(mau.get(i))));
                kt++;
            }
            if(kt == 1){
                lnChuThich2.setVisibility(View.VISIBLE);
            }else if(kt == 2){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
            }else if(kt == 3){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
            }else{
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
                lnChuThich5.setVisibility(View.VISIBLE);
            }
        }
        else{
            int kt = 0;
            long b = 0;
            for(int i = 0; i < list.size(); i++){
                List<DanhMucChi> danhMucChiList = UserDatabase.getInstance(getContext())
                        .danhMucChiDAO().danhSachTheoId(list.get(i).getKey());
                Double a = ((double)list.get(i).getValue() / (double) tongTien) * 100;
                String tien = String.format("%.2f", a);
                if(i == 0){
                    tvPhanTramA.setText(tien + "%");
                    tvTenA.setText(danhMucChiList.get(0).getName());
                    tvTienA.setText(list.get(0).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucChiList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#FFA726")));
                    kt++;
                }
                else if(i == 1){
                    tvPhanTramB.setText(tien + "%");
                    tvTenB.setText(danhMucChiList.get(0).getName());
                    tvTienB.setText(list.get(1).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucChiList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#66BB6A")));
                    kt++;
                }
                else if(i == 2){
                    tvPhanTramC.setText(tien + "%");
                    tvTenC.setText(danhMucChiList.get(0).getName());
                    tvTienC.setText(list.get(2).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucChiList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#EF5350")));
                    kt++;
                }
                else if(i == 3){
                    tvPhanTramD.setText(tien + "%");
                    tvTenD.setText(danhMucChiList.get(0).getName());
                    tvTienD.setText(list.get(3).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucChiList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#29B6F6")));
                    kt++;
                } else{
                    b += list.get(i).getValue();
                    kt++;
                }
            }
            String tien = String.format("%.2f", (b / (tongTien * 1.0)) * 100);
            tvPhanTramKhac.setText(tien + "%");
            tvTenKhac.setText("Khác");
            tvTienKhac.setText(b + " đ");
            pieChart.addPieSlice(
                    new PieModel(
                            "Khác",
                            (float) Double.parseDouble(tien),
                            Color.parseColor("#9F9F9E")));
            if(kt == 1){
                lnChuThich2.setVisibility(View.VISIBLE);
            }else if(kt == 2){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
            }else if(kt == 3){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
            }else if(kt == 4){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
                lnChuThich5.setVisibility(View.VISIBLE);
            }else{
                lnChuThich1.setVisibility(View.VISIBLE);
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
                lnChuThich5.setVisibility(View.VISIBLE);
            }
        }
    }

    private void xuLyChi() {
        List<ItemKhoanChi> listChi = UserDatabase.getInstance(getContext()).khoanChiDAO().BieuDo(this.id_user);
        if (!listChi.isEmpty()) {
            List<ItemKhoanChi> list1 = new ArrayList<>(); // danh sách theo tháng vào năm đã chọn
            Set<Integer> listIdDanhMucChi = new HashSet<>(); //các id Danh Mục chi theo thời gian đã chọn
            for (int i = 0; i < listChi.size(); i++) {
                int thang = Integer.parseInt(listChi.get(i).getNgaychi().substring(3, 5));
                int nam = Integer.parseInt(listChi.get(i).getNgaychi().substring(6));
                if (thang == this.month && nam == this.year) {
                    list1.add(listChi.get(i));
                    int idDanhMucChi = listChi.get(i).getId_danhmucchi();
                    listIdDanhMucChi.add(idDanhMucChi);
                }
            }
            List<Integer> list2 = new ArrayList<>();
            list2.addAll(listIdDanhMucChi);
            List<Long> listTongTien = new ArrayList<>(); //tổng tiền của từng id danh mục chi theo tháng và năm
            if (!list2.isEmpty()) {
                for (int i = 0; i < list2.size(); i++) {
                    long tongTien = 0;
                    for (int j = 0; j < list1.size(); j++) {
                        if (list1.get(j).getId_danhmucchi() == list2.get(i)) {
                            tongTien += list1.get(j).getSotien();
                        }
                    }
                    listTongTien.add(tongTien);
                }
            }
            if (!listTongTien.isEmpty()) {
                setDataChartChi(listTongTien, list2);
            }
            // thời gian không phát sinh chi tiêu
            else{
                Toast.makeText(getContext(), "Thời gian này bạn không có giao dịch gì",
                        Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getContext(), "Thời gian này bạn không có giao dịch gì",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void setDataChartThu(List<Long> listTongTien, List<Integer> list2) {
        for (long tong : listTongTien) {
            tongTien += tong;
        }

        Map<Integer, Long> map = new HashMap<Integer, Long>();
        for(int i = 0; i < list2.size(); i++){
            map.put(list2.get(i), listTongTien.get(i));
        }
        // sắp xếp giảm dần theo số tiền
        List<Map.Entry<Integer, Long>> list = new ArrayList<Map.Entry<Integer, Long>>();
        list.addAll(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Long>>() {
            @Override
            public int compare(Map.Entry<Integer, Long> o1, Map.Entry<Integer, Long> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        if(list2.size() < 5){
            int kt = 0;
            List<String> mau = new ArrayList<>();
            mau.add("#FFA726");
            mau.add("#66BB6A");
            mau.add("#EF5350");
            mau.add("#29B6F6");
            mau.add("#9F9F9E");
            for(int i = 0; i < listTongTien.size(); i++){
                List<DanhMucThu> danhMucThuList = UserDatabase.getInstance(getContext())
                        .danhMucThuDAO().danhSachTheoId(list2.get(i));
                Double a = ((double)list.get(i).getValue() / (double) tongTien) * 100;
                String tien = String.format("%.2f", a);
                if(i == 0){
                    tvPhanTramA.setText(tien + "%");
                    tvTenA.setText(danhMucThuList.get(0).getName());
                    tvTienA.setText(list.get(0).getValue() + " đ");
                }
                else if(i == 1){
                    tvPhanTramB.setText(tien + "%");
                    tvTenB.setText(danhMucThuList.get(0).getName());
                    tvTienB.setText(list.get(1).getValue() + " đ");
                }
                else if(i == 2){
                    tvPhanTramC.setText(tien + "%");
                    tvTenC.setText(danhMucThuList.get(0).getName());
                    tvTienC.setText(list.get(2).getValue() + " đ");
                }
                else if(i == 3){
                    tvPhanTramD.setText(tien + "%");
                    tvTenD.setText(danhMucThuList.get(0).getName());
                    tvTienD.setText(list.get(3).getValue() + " đ");
                }
                pieChart.addPieSlice(
                        new PieModel(
                                danhMucThuList.get(0).getName(),
                                (float) Double.parseDouble(tien),
                                Color.parseColor(mau.get(i))));
                kt++;
            }
            if(kt == 1){
                lnChuThich2.setVisibility(View.VISIBLE);
            }else if(kt == 2){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
            }else if(kt == 3){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
            }else{
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
                lnChuThich5.setVisibility(View.VISIBLE);
            }
        }
        else{
            int kt = 0;
            long b = 0;
            for(int i = 0; i < list.size(); i++){
                List<DanhMucThu> danhMucThuList = UserDatabase.getInstance(getContext())
                        .danhMucThuDAO().danhSachTheoId(list.get(i).getKey());
                Double a = ((double)list.get(i).getValue() / (double) tongTien) * 100;
                String tien = String.format("%.2f", a);
                if(i == 0){
                    tvPhanTramA.setText(tien + "%");
                    tvTenA.setText(danhMucThuList.get(0).getName());
                    tvTienA.setText(list.get(0).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucThuList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#FFA726")));
                    kt++;
                }
                else if(i == 1){
                    tvPhanTramB.setText(tien + "%");
                    tvTenB.setText(danhMucThuList.get(0).getName());
                    tvTienB.setText(list.get(1).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucThuList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#66BB6A")));
                    kt++;
                }
                else if(i == 2){
                    tvPhanTramC.setText(tien + "%");
                    tvTenC.setText(danhMucThuList.get(0).getName());
                    tvTienC.setText(list.get(2).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucThuList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#EF5350")));
                    kt++;
                }
                else if(i == 3){
                    tvPhanTramD.setText(tien + "%");
                    tvTenD.setText(danhMucThuList.get(0).getName());
                    tvTienD.setText(list.get(3).getValue() + " đ");
                    pieChart.addPieSlice(
                            new PieModel(
                                    danhMucThuList.get(0).getName(),
                                    (float) Double.parseDouble(tien),
                                    Color.parseColor("#29B6F6")));
                    kt++;
                } else{
                    b += list.get(i).getValue();
                    kt++;
                }
            }
            String tien = String.format("%.2f", (b / (tongTien * 1.0)) * 100);
            tvPhanTramKhac.setText(tien + "%");
            tvTenKhac.setText("Khác");
            tvTienKhac.setText(b + " đ");
            pieChart.addPieSlice(
                    new PieModel(
                            "Khác",
                            (float) Double.parseDouble(tien),
                            Color.parseColor("#9F9F9E")));
            if(kt == 1){
                lnChuThich2.setVisibility(View.VISIBLE);
            }else if(kt == 2){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
            }else if(kt == 3){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
            }else if(kt == 4){
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
                lnChuThich5.setVisibility(View.VISIBLE);
            }else{
                lnChuThich1.setVisibility(View.VISIBLE);
                lnChuThich2.setVisibility(View.VISIBLE);
                lnChuThich3.setVisibility(View.VISIBLE);
                lnChuThich4.setVisibility(View.VISIBLE);
                lnChuThich5.setVisibility(View.VISIBLE);
            }
        }
    }

    private void xuLyThu() {
        List<ItemDoanhThu> listThu = UserDatabase.getInstance(getContext()).doanhThuDAO().BieuDo(this.id_user);
        if (!listThu.isEmpty()) {
            List<ItemDoanhThu> list1 = new ArrayList<>(); // danh sách theo tháng vào năm đã chọn
            Set<Integer> listIdDanhMucThu = new HashSet<>(); //các id Danh Mục chi theo thời gian đã chọn
            for (int i = 0; i < listThu.size(); i++) {
                int thang = Integer.parseInt(listThu.get(i).getNgaythu().substring(3, 5));
                int nam = Integer.parseInt(listThu.get(i).getNgaythu().substring(6));
                if (thang == this.month && nam == this.year) {
                    list1.add(listThu.get(i));
                    int idDanhMucThu = listThu.get(i).getId_danhmucthu();
                    listIdDanhMucThu.add(idDanhMucThu);
                }
            }
            List<Integer> list2 = new ArrayList<>();
            list2.addAll(listIdDanhMucThu);
            List<Long> listTongTien = new ArrayList<>(); //tổng tiền của từng id danh mục chi theo tháng và năm
            if (!list2.isEmpty()) {
                for (int i = 0; i < list2.size(); i++) {
                    long tongTien = 0;
                    for (int j = 0; j < list1.size(); j++) {
                        if (list1.get(j).getId_danhmucthu() == list2.get(i)) {
                            tongTien += list1.get(j).getSotien();
                        }
                    }
                    listTongTien.add(tongTien);
                }
            }
            if (!listTongTien.isEmpty()) {
                setDataChartThu(listTongTien, list2);
            }
            // thời gian không phát sinh chi tiêu
            else{
                Toast.makeText(getContext(), "Thời gian này bạn không có giao dịch gì",
                        Toast.LENGTH_LONG).show();
            }
        }
        else{
            Toast.makeText(getContext(), "Thời gian này bạn không có giao dịch gì",
                    Toast.LENGTH_LONG).show();
        }
    }

    // ban đầu khi chưa chọn thời gian thì ẩn các View chú thích
    private void anViewChuThichBanDau(){
        lnChuThich1.setVisibility(View.GONE);
        lnChuThich2.setVisibility(View.GONE);
        lnChuThich3.setVisibility(View.GONE);
        lnChuThich4.setVisibility(View.GONE);
        lnChuThich5.setVisibility(View.GONE);
    }
}
